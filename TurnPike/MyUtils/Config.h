//
//  Common.h
//
//  Created by ChungJin.Sim on 9/6/13.
//  Copyright (c) 2013 PIC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Config : NSObject {
}
@property (nonatomic, readwrite) long privilege;

+ (void) setLoginName : (NSString*)newLoginName;
+ (NSString*) loginName;

+ (void) setLoginPassword : (NSString*)newLoginPassword;
+ (NSString*) loginPassword;

+ (void) setDepartment : (NSString *)department;
+ (NSString *) loginDepartment;

+ (void) setAutoLogin : (BOOL) bAuto;
+ (BOOL) isAutoLogin;

+ (void) setLicense : (BOOL) bAccept;
+ (BOOL) isLicense;

+ (void) setSpeed : (double)speed;
+ (double) getSpeed;

+ (void) setAlarmMinutes : (long)value;
+ (long) getAlarmMinutes;

@end
