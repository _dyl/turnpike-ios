//
//  Common.m
//
//  Created by R CJ on 1/5/13.
//  Copyright (c) 2013 PIC. All rights reserved.
//

#import "Config.h"

#define KEY_LOGIN_NAME              @"loginName"
#define KEY_LOGIN_PASSWORD          @"loginPassword"
#define KEY_LOGIN_DEPARTMENT		@"loginDepartment"
#define KEY_AUTOLOGIN               @"autoLogin"

#define KEY_LICENSE                 @"license"

@implementation Config

+ (void) setLoginName : (NSString*)newLoginName
{
    [[NSUserDefaults standardUserDefaults] setObject:newLoginName forKey:KEY_LOGIN_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*) loginName
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:KEY_LOGIN_NAME];
}

+ (void) setLoginPassword : (NSString*)newLoginPassword
{
    [[NSUserDefaults standardUserDefaults] setObject:newLoginPassword forKey:KEY_LOGIN_PASSWORD];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*) loginPassword
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:KEY_LOGIN_PASSWORD];
}

+ (void) setDepartment : (NSString *)department
{
    [[NSUserDefaults standardUserDefaults] setObject:department forKey:KEY_LOGIN_DEPARTMENT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *) loginDepartment
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:KEY_LOGIN_DEPARTMENT];
}

+ (void) setAutoLogin : (BOOL) bAuto
{
    [[NSUserDefaults standardUserDefaults] setBool:bAuto forKey:KEY_AUTOLOGIN];
}

+ (BOOL) isAutoLogin
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:KEY_AUTOLOGIN];
}

+ (void) setLicense : (BOOL) bAccept
{
    [[NSUserDefaults standardUserDefaults] setBool:bAccept forKey:KEY_LICENSE];
}

+ (BOOL) isLicense
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:KEY_LICENSE];
}

+ (void) setSpeed : (double)speed
{
	[[NSUserDefaults standardUserDefaults] setDouble:speed forKey:@"speed"];
}

+ (double) getSpeed
{
	return [[NSUserDefaults standardUserDefaults] doubleForKey:@"speed"];
}

+ (void) setAlarmMinutes : (long)value
{
	[[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"AlarmMinutes"];
}

+ (long) getAlarmMinutes
{
	return [[NSUserDefaults standardUserDefaults] integerForKey:@"AlarmMinutes"];
}

@end
