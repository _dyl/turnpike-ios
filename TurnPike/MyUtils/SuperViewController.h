//
//  SuperViewController.h
//  BJPinChe
//
//  Created by DaYong Li on 10/30/14.
//  Copyright (c) 2014 MICA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

- (void) initInputControls;
- (void) duplicateUser:(NSString *)result;
- (void) duplicateLogout;

- (void) forceLogout : (UIViewController *)login;

- (IBAction)onTapBack:(id)sender;

@end
