//
//  SuperTableViewController.h
//  DingSheng
//
//  Created by DaYong Li on 10/30/14.
//  Copyright (c) 2014 MICA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperTableViewController : UITableViewController <UITextFieldDelegate, UITextViewDelegate>

- (IBAction)onTapBack:(id)sender;

@end
