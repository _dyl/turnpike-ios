//
//  SuperViewController.m
//  BJPinChe
//
//  Created by DaYong Li on 10/30/14.
//  Copyright (c) 2014 MICA. All rights reserved.
//

#import "SuperViewController.h"
//#import "Config.h"

@interface SuperViewController ()

@end

@implementation SuperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 * Initilaize all input controls 
 */
- (void) initInputControls
{
    // make tap recognizer to hide keyboard
    UITapGestureRecognizer * tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapBackGround:)];
    
    [self.view addGestureRecognizer:tapRecognizer];
}

/**
 * Hide keyboard when tapped background
 */
- (void) handleTapBackGround:(id)sender
{
    [self.view endEditing:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // find next responder
    UIResponder * nextResp = [self.view viewWithTag:nextTag];
    if (nextResp) {
        // set it
        [nextResp becomeFirstResponder];
    } else {
        // hide keyboard
        [textField resignFirstResponder];
    }
    
    return NO;
}

/**
 * detect duplicate user
 * @param : result [in], error state message
 */
- (void) duplicateUser:(NSString *)result
{
    NSLog(@"Detect duplicate user");
    
//    [SVProgressHUD dismissWithError:result afterDelay:DEF_DELAY];
//    [self performSelector:@selector(duplicateLogout) withObject:nil afterDelay:DEF_DELAY];
}

- (void) duplicateLogout
{
//    // remove account data
//    [Common setEmployeeInfo:nil];
//    
//    // go to main view controller
//    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"maintab"];
//    [self presentViewController:viewController animated:YES completion:nil];
}

- (void) forceLogout : (UIViewController *)login
{
	// get current key window
//	UIWindow * window = [UIApplication sharedApplication].keyWindow;
//	// get its rootviewcontroller
//	UIViewController * root = window.rootViewController;
//	[window setRootViewController:nil];
//	[window setRootViewController:login];
	
//	[UIView transitionFromView:self.view toView:login.view duration:0.5 options:UIViewAnimationOptionCurveEaseIn completion:^(BOOL finish)
//	{
//		[[[self presentingViewController] presentingViewController] dismissViewControllerAnimated:NO completion:nil];
//	}];
	
	// set custom animation
	CATransition * transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionMoveIn;
	transition.subtype = kCATransitionFromLeft;  		// kCATransitionFromRight
	
	UIView * containerView = self.view.window;
	[containerView.layer addAnimation:transition forKey:nil];
	
	// get top presenting view controller
	UIViewController * topCtrl = self;
	while (topCtrl.presentingViewController) {
		topCtrl = topCtrl.presentingViewController;
	}
	[topCtrl dismissViewControllerAnimated:NO completion:nil];
	
	// check child view controllers & go to login
	NSMutableArray * newArray = [[NSMutableArray alloc] initWithObjects:login, nil];
	[topCtrl.childViewControllers[0] navigationController].viewControllers = newArray;
}

- (IBAction)onTapBack:(id)sender {
	
    if (self.navigationController.viewControllers.count > 1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}


@end
