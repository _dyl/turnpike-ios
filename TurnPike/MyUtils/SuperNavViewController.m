//
//  SuperNavViewController.m
//  DingSheng
//
//  Created by DaYong Li on 10/30/14.
//  Copyright (c) 2014 MICA. All rights reserved.
//

#import "SuperNavViewController.h"
#import "GlobalFunc.h"

@interface SuperNavViewController ()

@end

@implementation SuperNavViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([GlobalFunc isOverIOS7])
    {
        self.navigationBar.tintColor = [UIColor whiteColor];
    }
    else
    {
        self.navigationBar.tintColor = [UIColor colorWithRed:92.0/255.0 green:165.0/255.0 blue:220.0/255.0 alpha:1];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
