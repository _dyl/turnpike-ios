//
//  AppDelegate.h
//  TurnPike
//
//  Created by b on 4/17/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

