//
//  ViewController.m
//  TurnPike
//
//  Created by b on 4/17/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import "RootViewController.h"
#import "MapViewController.h"
#import "ListViewController.h"
#import "UpgradeViewController.h"
#import "Config.h"
#import "DataManager.h"
#import "GlobalFunc.h"
#import "SVProgressHUD.h"

static RootViewController *instance = nil;

@interface RootViewController ()
{
    MapViewController *mapViewer;
    ListViewController *lstViewer;
    UpgradeViewController *ugdViewer;
    int srvType;
    int viewSwitch;
	
	CLLocationManager *locationManager;
	CLGeocoder *geoCoder;
	CLPlacemark *placemark;

}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintStatusBarHeight;
@property (weak, nonatomic) IBOutlet UIView *vwTabBar;
@property (weak, nonatomic) IBOutlet UIView *vwLicense;
@property (weak, nonatomic) IBOutlet UIView *vwMainView;
@property (weak, nonatomic) IBOutlet UIWebView *vwWarningMsg;

@end

@implementation RootViewController

+(id) shareInstance
{
    return instance;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    if (instance){
        self = instance;
        return instance;
    }
    
    self = [super initWithCoder:aDecoder];
    if (!self) return nil;
    
    instance = self;
    return self;
}

-(void) setServiceType:(int)type  which:(int)which
{
    srvType = type;
    viewSwitch = which;
}

- (void) setCurrentMarker:(MarkerData*)marker
{
	[mapViewer setCurrentMarker:marker];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[self startLocationManager];
	
    [[DataManager shareObject] loadData];
	
    [self.navigationController setNavigationBarHidden:YES];

    self.vwTabBar.hidden = YES;
    self.vwLicense.hidden = YES;
    self.vwMainView.hidden = YES;
    
    if (NO && [Config isLicense])
    {
        self.vwLicense.hidden = YES;
        self.vwMainView.hidden = NO;
    }else{
        self.vwLicense.hidden = NO;
        self.vwMainView.hidden = YES;
    }

    [self setNeedsStatusBarAppearanceUpdate];
    if ([GlobalFunc getSystemVersion] < 7.0){
        _constraintStatusBarHeight.constant = 0;
    }
	
	NSString *localURL = [[NSBundle mainBundle] pathForResource:@"warning" ofType:@"html" inDirectory:nil];
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:localURL]];
	[_vwWarningMsg loadRequest:urlRequest];

	[_vwWarningMsg setBackgroundColor:[UIColor clearColor]];
	[_vwWarningMsg setOpaque:NO];
	
    mapViewer = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MapViewController"];
    lstViewer = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListViewController"];
    ugdViewer = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"UpgradeViewController"];
}

-(void) viewWillAppear:(BOOL)animated{
    if (viewSwitch == SWITCH_LIST){
        [self showList:NO];
        return;
    }else if (viewSwitch == SWITCH_MAP){
        [self showMap:NO];
        return;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle) preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (IBAction)onAccept:(id)sender {
    [Config setLicense:YES];
    
    self.vwMainView.hidden = NO;
    self.vwLicense.hidden = YES;
}

- (IBAction)onDecline:(id)sender {
    [Config setLicense:NO];
}

#pragma mark - Main Button Handler

- (IBAction)onMapAll:(id)sender {
    srvType = SERVICE_ALL;
    [self showMap:YES];
}

- (IBAction)onMapPlaza:(id)sender {
    srvType = SERVICE_PLAZA;
    [self showMap:YES];
}

- (IBAction)onMapExits:(id)sender {
    srvType = SERVICE_EXIT;
    [self showMap:YES];
}

- (IBAction)onTimeSpeed:(id)sender {
    srvType = SERVICE_SPEEDTIME;
    [self showMap:YES];
}

- (IBAction)onListAll:(id)sender {
    srvType = SERVICE_ALL;
    [self showList:YES];
}

- (IBAction)onListPlaza:(id)sender {
    srvType = SERVICE_PLAZA;
    [self showList:YES];
}

- (IBAction)onListExits:(id)sender {
    srvType = SERVICE_EXIT;
    [self showList:YES];
}

- (IBAction)onTSList:(id)sender {
    srvType = SERVICE_SPEEDTIME;
    [self showList:YES];
}

- (IBAction)onUpgrade:(id)sender {
    if (!ugdViewer) return;
    
    [self.navigationController pushViewController:ugdViewer animated:YES];
}

-(void) showMap :(BOOL)animaton
{
    if (!mapViewer) return;
    
    [mapViewer setServiceType:srvType];
    viewSwitch = SWITCH_NONE;
    [self.navigationController pushViewController:mapViewer animated:animaton];
}

-(void) showList :(BOOL)animaton
{
    if (!lstViewer) return;
    
    [lstViewer setServiceType:srvType];
    viewSwitch = SWITCH_NONE;
    [self.navigationController pushViewController:lstViewer animated:animaton];
}

#pragma mark - CLLocation relative

- (void) startLocationManager
{
	if (locationManager != nil)
	{
		return;
	}
	locationManager = [[CLLocationManager alloc] init];
	locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
	locationManager.activityType = CLActivityTypeAutomotiveNavigation;
	
	locationManager.delegate = self;
	
	if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
		[locationManager requestWhenInUseAuthorization];
	}
	[locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	CLLocation *currentLocation = (CLLocation*)[locations lastObject];
	NSLog(@"location info object=%@", currentLocation);
	
	[DataManager setCurrentPosition:CGPointMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)];
		
#if 0
	// Reverse Geocoding
	NSLog(@"Resolving the Address");
	[geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
		NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
		if (error == nil && [placemarks count] > 0) {
			placemark = [placemarks lastObject];
			NSLog(@"location info object=%@", [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
											   placemark.subThoroughfare, placemark.thoroughfare,
											   placemark.postalCode, placemark.locality,
											   placemark.administrativeArea,
											   placemark.country]);
		} else {
			NSLog(@"%@", error.debugDescription);
		}
	} ];
#endif
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
}

- (void)requestAlwaysAuthorization
{
	CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
	
	// If the status is denied or only granted for when in use, display an alert
	if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
		NSString *title;
		title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
		NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
		
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
															message:message
														   delegate:self
												  cancelButtonTitle:@"Cancel"
												  otherButtonTitles:@"Settings", nil];
		[alertView show];
	}
	// The user has not enabled any location services. Request background authorization.
	else if (status == kCLAuthorizationStatusNotDetermined) {
		[locationManager requestAlwaysAuthorization];
	}
}

@end
