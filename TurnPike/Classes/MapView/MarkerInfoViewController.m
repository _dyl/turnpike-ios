//
//  MarkerInfoViewController.m
//  TurnPike
//
//  Created by rangdang on 4/26/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import "MarkerInfoViewController.h"
#import "GlobalFunc.h"

@interface MarkerInfoViewController ()
{
    MarkerData* markInfo;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintStatusBarHeight;
@property (weak, nonatomic) IBOutlet UIWebView *vwWebView;

@end

@implementation MarkerInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];
    if ([GlobalFunc getSystemVersion] < 7.0){
        _constraintStatusBarHeight.constant = 0;
    }

    [_vwWebView setBackgroundColor:[UIColor clearColor]];
    [_vwWebView setOpaque:NO];
    [self setMarker:markInfo];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle) preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Utillites
-(void) setMarker : (MarkerData*)marker
{
    if (!marker) return;
    markInfo = marker;
    
    if (markInfo.type == MARK_PLAZA)
    {
        NSString *localURL = [[NSBundle mainBundle] pathForResource:marker.detail ofType:@"html" inDirectory:nil];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:localURL]];
        [_vwWebView loadRequest:urlRequest];

    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
