//
//  MapViewController.h
//  TurnPike
//
//  Created by rangdang on 4/23/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@class MarkerData;

@interface MapViewController : SuperViewController
- (void) setServiceType:(int)type;
- (void) setCurrentMarker:(MarkerData*)marker;
@end
