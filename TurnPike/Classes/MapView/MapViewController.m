//
//  MapViewController.m
//  TurnPike
//
//  Created by rangdang on 4/23/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import "MapViewController.h"
#import "GlobalFunc.h"
#import "Datamanager.h"
#import "RootViewController.h"
#import "MarkerInfoViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Toast+UIView.h"
#import "Config.h"

#define MAX_MPH		500
#define MIN_MPH		10

#define MAX_MINUTES_VALUE		10000
#define MIN_MINUTES_VALUE		0

@interface MapViewController ()<GMSMapViewDelegate, UITextFieldDelegate>
{
	BOOL		moveMap;
	MarkerData *currentMarker;
	
    UIImage *plaza, *exit;

    MarkerInfoViewController *markInfoViewCtrl;
    NSMutableArray *markers;
    
    int serviceType;
	
	BOOL firstLocationUpdate_;
}

@property (weak, nonatomic) IBOutlet GMSMapView *MapView_;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintStatusBarHeight;

@property (weak, nonatomic) IBOutlet UIImageView *imgPlazaMark;
@property (weak, nonatomic) IBOutlet UIView *vwSpeedTime;
@property (weak, nonatomic) IBOutlet UILabel *lblMarkerName;
@property (weak, nonatomic) IBOutlet UIButton *btnBell;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UITextField *txtSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIButton *btnNormal;
@property (weak, nonatomic) IBOutlet UIButton *btnSatellite;
@property (weak, nonatomic) IBOutlet UIButton *btnHybrid;
@property (weak, nonatomic) IBOutlet UIButton *btnTerrain;

@property (weak, nonatomic) IBOutlet UIView *vwSpeed;
@property (weak, nonatomic) IBOutlet UITextField *txtMPH;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedUnit;

@end

@implementation MapViewController

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (!self)
        return nil;
    
	serviceType = SERVICE_ALL;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	[self initControls];
}

- (void) initControls
{
	[self loadMarkers];
	
	plaza = [UIImage imageNamed:@"icon_plaza"];
	exit = [UIImage imageNamed:@"icon_exit"];
	
	double speed = [Config getSpeed];
	if (speed < 50)
		speed = 50;
	else if (speed > MAX_MPH)
		speed = MAX_MPH;
	[Config setSpeed:speed];

	long min = [Config getAlarmMinutes];
	if (min < MIN_MINUTES_VALUE)
		min = MIN_MINUTES_VALUE;
	else if (min > MAX_MINUTES_VALUE)
		min = MAX_MINUTES_VALUE;
	[Config setAlarmMinutes:min];

	_txtMPH.text = [NSString stringWithFormat:@"%ld", (long)speed];
	
	markInfoViewCtrl = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MarkerInfoViewController"];
	
	// Do any additional setup after loading the view.
	[self setNeedsStatusBarAppearanceUpdate];
	if ([GlobalFunc getSystemVersion] < 7.0){
		_constraintStatusBarHeight.constant = 0;
	}
	
	// Create a GMSCameraPosition that tells the map to display the
	// coordinate -33.86,151.20 at zoom level 6.
	GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:27.6013
															longitude:-80.8224
																 zoom:7];
	[self.MapView_ setCamera:camera];
	self.MapView_.myLocationEnabled = YES;
	
	[self.MapView_ addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context:NULL];
	
	self.MapView_.delegate = self;
	self.MapView_.settings.myLocationButton = YES;
	self.MapView_.settings.zoomGestures = YES;
	self.MapView_.settings.compassButton = YES;
	
	dispatch_async(dispatch_get_main_queue(), ^{
		self.MapView_.myLocationEnabled = YES;
	});
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
	if (!firstLocationUpdate_)
	{
		firstLocationUpdate_ = YES;
		self.MapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:14];
		
	}else{
		[DataManager setCurrentPosition:CGPointMake(location.coordinate.latitude, location.coordinate.longitude)];
	}
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	// register for keyboard notifications
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillShow:)
												 name:UIKeyboardWillShowNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillHide:)
												 name:UIKeyboardWillHideNotification
											   object:nil];

    [self setMarkers];
	
	if (moveMap)
	{
		moveMap = NO;
		GMSCameraPosition *old = self.MapView_.camera;
		GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentMarker.latitude
																longitude:currentMarker.longitude
																	 zoom:old.zoom
																  bearing:old.bearing
															 viewingAngle:old.viewingAngle];
		[self.MapView_ setCamera:camera];
		
		GMSMarker *mark = (GMSMarker *)currentMarker.object;
		self.MapView_.selectedMarker = mark;
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	// unregister for keyboard notifications while not visible.
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillShowNotification
												  object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillHideNotification
												  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle) preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (IBAction)onTapList:(id)sender
{
    RootViewController *rootView = [RootViewController shareInstance];
    [rootView setServiceType:serviceType which:SWITCH_LIST];

    [self.navigationController popViewControllerAnimated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - User Event
- (IBAction)onTapNormal:(id)sender
{
	_btnNormal.selected = YES;
	_btnSatellite.selected = NO;
	_btnHybrid.selected = NO;
	_btnTerrain.selected = NO;
	
	_MapView_.mapType = kGMSTypeNormal;
}

- (IBAction)onTapSatellite:(id)sender
{
	_btnNormal.selected = NO;
	_btnSatellite.selected = YES;
	_btnHybrid.selected = NO;
	_btnTerrain.selected = NO;
	
	_MapView_.mapType = kGMSTypeSatellite;
}

- (IBAction)onTapHybrid:(id)sender
{
	_btnNormal.selected = NO;
	_btnSatellite.selected = NO;
	_btnHybrid.selected = YES;
	_btnTerrain.selected = NO;
	
	_MapView_.mapType = kGMSTypeHybrid;
}

- (IBAction)onTapTerrain:(id)sender
{
	_btnNormal.selected = NO;
	_btnSatellite.selected = NO;
	_btnHybrid.selected = NO;
	_btnTerrain.selected = YES;
	
	_MapView_.mapType = kGMSTypeTerrain;
}

#pragma mark - Utillites

-(void) loadMarkers
{
    markers = [[DataManager shareObject] getMarkers];
}

- (void) setServiceType:(int)type
{
    serviceType = type;
}

- (void) setCurrentMarker:(MarkerData*)marker;{
	moveMap = YES;
	currentMarker = marker;
}

-(void) setMarkers
{
    [self.MapView_ clear];
    
    if (serviceType == SERVICE_PLAZA)
    {
        _lblTitle.text = @"PLAZA MAP";
        for (MarkerData *mark in markers) {
            if (mark.type != MARK_PLAZA)
                continue;
            
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(mark.latitude, mark.longitude);
            marker.title = [NSString stringWithFormat:@"MM-%@", mark.title];
            marker.snippet = mark.marker_desc;
            marker.icon = plaza;
            marker.groundAnchor = CGPointMake(0.5, 0.5);
            marker.userData = mark;
            marker.map = self.MapView_;

			mark.object = marker;
        }
    }else if (serviceType == SERVICE_EXIT)
    {
        _lblTitle.text = @"EXIT MAP";
        for (MarkerData *mark in markers) {
            if (mark.type != MARK_EXIT)
                continue;
            
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(mark.latitude, mark.longitude);
            marker.title = [NSString stringWithFormat:@"MM-%@", mark.title];
            marker.snippet = mark.marker_desc;
            marker.icon = exit;
            marker.groundAnchor = CGPointMake(0.5, 0.5);
            marker.userData = mark;
            
            marker.map = self.MapView_;
			
			mark.object = marker;
        }
    }else if (serviceType == SERVICE_ALL){
        _lblTitle.text = @"ALL MAP";
        for (MarkerData *mark in markers) {
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(mark.latitude, mark.longitude);
            marker.title = [NSString stringWithFormat:@"MM-%@", mark.title];
            marker.snippet = mark.marker_desc;
            marker.icon = mark.type==MARK_PLAZA?plaza:exit;
            marker.groundAnchor = CGPointMake(0.5, 0.5);
            marker.userData = mark;
            
            marker.map = self.MapView_;
			
			mark.object = marker;
        }
    }else{
        _lblTitle.text = @"TIME / SPEED";
        for (MarkerData *mark in markers) {
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(mark.latitude, mark.longitude);
            marker.title = [NSString stringWithFormat:@"MM-%@", mark.title];
            marker.snippet = mark.marker_desc;
            marker.icon = mark.type==MARK_PLAZA?plaza:exit;
            marker.groundAnchor = CGPointMake(0.5, 0.5);
            marker.userData = mark;
            if (mark.type == MARK_PLAZA)
                marker.infoWindowAnchor = CGPointMake(0.615, 0.15);
            marker.map = self.MapView_;
			
			mark.object = marker;
        }
    }
}


#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	GMSMarker *marker = self.MapView_.selectedMarker;
	if (!marker) return;
	MarkerData *markInfo =marker.userData;
	
	if (!markInfo) return;

	if (buttonIndex == 1)//set alarm
	{
		[[DataManager shareObject] setAlarm:markInfo view:self.view];
		self.MapView_.selectedMarker = NULL;
	}
	else if (buttonIndex == 2)//show detail
	{
		[self.navigationController pushViewController:markInfoViewCtrl animated:YES];
		[markInfoViewCtrl setMarker:markInfo];
	}
}


#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
	[self.view endEditing:YES];
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
	[self.view endEditing:YES];
//    if (serviceType == SERVICE_SPEEDTIME)
//        return;
	
    MarkerData *markInfo =marker.userData;
	
	if (serviceType == SERVICE_SPEEDTIME && markInfo.distance > 0)
	{
		[[DataManager shareObject] setAlarm:markInfo view:self.view];
		GMSMarker *gmarker = self.MapView_.selectedMarker;
		self.MapView_.selectedMarker = NULL;
		self.MapView_.selectedMarker = gmarker;
		return;
	}
	if (markInfo.type == MARK_EXIT)
		return;
	
	[self.navigationController pushViewController:markInfoViewCtrl animated:YES];
	[markInfoViewCtrl setMarker:markInfo];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
	[self.view endEditing:YES];
    if (serviceType != SERVICE_SPEEDTIME)
        return nil;
    
    MarkerData *markInfo = marker.userData;
	double speed = [Config getSpeed];
	
	_lblMarkerName.text = markInfo.marker_desc;
	_txtSpeed.text = [NSString stringWithFormat:@"%ld", (long)speed];
	_imgPlazaMark.hidden = markInfo.type != MARK_PLAZA;

	if ([[DataManager shareObject] calculateGeoInfoByMPH:speed marker:markInfo] && markInfo.distance > 0)
	{
		_lblDistance.text = markInfo.mile_desc;
		_lblTime.text = markInfo.time_desc;
	}else{
		_btnBell.enabled = NO;
		_lblDistance.text = @"N/A";
		_lblTime.text = @"N/A";
		[self.view makeToast:@"Cannot calculate distance.\nMaybe network is not working or you are out of Florida."];
	}
	
	NSArray *notis = [[UIApplication sharedApplication] scheduledLocalNotifications];
	
	if (markInfo.notification)
	{
		bool bExit = false;
		for (UILocalNotification *notify in notis)
		{
			if (!notify.alertAction)
				continue;
			
			long nId = [notify.alertAction integerValue];
			if (nId == markInfo.nID)
			{
				bExit = true;
				break;
			}
		}
		if (!bExit)
			markInfo.notification = nil;
	}

	if (!markInfo.notification)
	{
		[_btnBell setImage:[UIImage imageNamed:@"alarm_off"] forState:UIControlStateNormal];
	}else{
		[_btnBell setImage:[UIImage imageNamed:@"alarm_on"] forState:UIControlStateNormal];
	}
    return _vwSpeedTime;
}

- (IBAction)onSetMPH:(id)sender
{
	_MapView_.selectedMarker = nil;
	
	_speedLabel.text = @"Enter Speed Here!!!";
	_speedUnit.text = @"MPH";
	_txtMPH.text = [NSString stringWithFormat:@"%ld", (long)[Config getSpeed]];
	
	if (serviceType != SERVICE_SPEEDTIME)
	{
		_vwSpeed.hidden = !_vwSpeed.hidden;
		return;
	}
	_vwSpeed.hidden = NO;
}

- (IBAction)onSetAlarmTime:(id)sender
{
	_MapView_.selectedMarker = nil;
	
	_speedLabel.text = @"Enter Minutes Here!!!";
	_speedUnit.text = @"MIN";
	
	_txtMPH.text = [NSString stringWithFormat:@"%ld", (long)[Config getAlarmMinutes]];
	if (serviceType != SERVICE_SPEEDTIME)
	{
		_vwSpeed.hidden = !_vwSpeed.hidden;
		return;
	}
	_vwSpeed.hidden = NO;
}

- (IBAction)onUseMPH:(id)sender
{
	NSString *strToast;
	
	if ([_speedUnit.text isEqualToString:@"MPH"])
	{
		double speed = [_txtMPH.text doubleValue];
		
		if (speed > MAX_MPH)
		{
			speed = MAX_MPH;
		}else if (speed < MIN_MPH)
			speed = MIN_MPH;
		
		[Config setSpeed:speed];
		
		strToast = [NSString stringWithFormat:@"Set up speed to %ldMPH", (long)speed];
	}else{
		long min = [_txtMPH.text integerValue];
		
		if (min < MIN_MINUTES_VALUE)
			min = MIN_MINUTES_VALUE;
		else if (min > MAX_MINUTES_VALUE)
			min = MAX_MINUTES_VALUE;
		[Config setAlarmMinutes:min];
		
		strToast = [NSString stringWithFormat:@"Set up Alarm Minutes to %ld minutes", (long)min];
	}
	[self.view endEditing:YES];
	_vwSpeed.hidden = YES;
	[self.view makeToast:strToast];
}

#pragma mark UITextFieldDelegate

#define kOFFSET_FOR_KEYBOARD 216

-(void)keyboardWillShow : (NSNotification*)notification
{
	NSDictionary*keyboardInfo = [notification userInfo];
	NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
	CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
	
	// Animate the current view out of the way
	//if (self.view.frame.origin.y >= 0)
	if ([_txtMPH isFirstResponder])
	{
		[self setViewMovedUp:YES delta:keyboardFrameBeginRect.size.height];
	}
}

-(void)keyboardWillHide : (NSNotification*)notification
{
	//if (self.view.frame.origin.y < 0)
	if ([_txtMPH isFirstResponder])
	{
		[self setViewMovedUp:NO delta:0];
	}
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp delta:(long)delta
{
	static long seed_delta;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3]; // if you want to slide up the view
	
	CGRect rect = self.view.frame;
	if (movedUp)
	{
		seed_delta = delta;
		rect.origin.y -= seed_delta;
	}
	else
	{
		// revert back to the normal state.
		rect.origin.y += seed_delta;
	}
	self.view.frame = rect;
	
	[UIView commitAnimations];
}


@end
