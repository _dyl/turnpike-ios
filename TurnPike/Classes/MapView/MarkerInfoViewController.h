//
//  MarkerInfoViewController.h
//  TurnPike
//
//  Created by rangdang on 4/26/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"
#import "DataManager.h"

@interface MarkerInfoViewController : SuperViewController
-(void) setMarker : (MarkerData*)marker;
@end
