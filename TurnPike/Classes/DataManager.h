//
//  DataManager.h
//  TurnPike
//
//  Created by rangdang on 4/24/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>

#define MARK_PLAZA  1
#define MARK_EXIT   2

#define ALARM_SOUND_FILE	@"alert-tone.mp3"

@interface MarkerData : NSObject
@property int nID;
@property (nonatomic, assign) int type;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, retain) NSString *marker_desc;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSString *title;

@property (nonatomic, retain) id					object;
@property (nonatomic, retain) UILocalNotification *	notification;

@property (nonatomic, readwrite) double distance;
@property (nonatomic, readwrite) double time;

@property (nonatomic, retain) NSString *distance_desc;
@property (nonatomic, retain) NSString *mile_desc;
@property (nonatomic, retain) NSString *time_desc;
@end

@interface DataManager : NSObject
+(DataManager*) shareObject;

+(CGFloat) getSystemVersion;

-(BOOL) loadData;
-(NSMutableArray*) getMarkers;
-(NSMutableArray*) getPlazas;
-(NSMutableArray*) getExits;
-(BOOL) refreshGeoInfoByMPH : (long)mph Markers:(NSMutableArray*)Markers;
-(BOOL) calculateGeoInfoByMPH : (long)mph marker:(MarkerData*)marker;
- (void) setAlarm : (MarkerData*)mark view:(UIView*)view;

+(CGPoint) getCurrentPosition;
+(void) setCurrentPosition:(CGPoint)position;
//+(long)getDistancebyVehicle : (double)lat1 lon1:(double)lon1 lat2:(double)lat2 lon2:(double)lon2;
+(BOOL)calcuGeoInformation:(MarkerData*)marker lat:(double)orglat lon:(double)orglon;

+(NSString*) makeTimeString:(long)seconds;
@end
