//
//  ListViewController.m
//  TurnPike
//
//  Created by rangdang on 4/23/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import "ListViewController.h"
#import "GlobalFunc.h"
#import "Datamanager.h"
#import "RootViewController.h"
#import "Toast+UIView.h"
#import "Config.h"

#pragma mark - Cell

@interface MarkerCell()
{
    MarkerData *markInfo;
}
@property (weak, nonatomic) IBOutlet UILabel *exit;
@property (weak, nonatomic) IBOutlet UILabel *information;

@property (weak, nonatomic) IBOutlet UIView *vwTimeSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration;


- (void) setMarker : (MarkerData*)mark service_type:(int)service_type;
@end

@implementation MarkerCell
- (void) setMarker : (MarkerData*)mark service_type:(int)service_type
{
    markInfo = mark;
    
    _exit.text = mark.title;
    _information.text = mark.marker_desc;
    
    self.contentView.backgroundColor = [UIColor clearColor];
	
    if (markInfo.type == MARK_PLAZA)
    {
        
        self.contentView.backgroundColor = [UIColor greenColor];
    }
	[_vwTimeSpeed setHidden:YES];
	if (service_type == SERVICE_SPEEDTIME && mark.distance > 0)
	{
		_vwTimeSpeed.hidden = NO;
		
		if (mark.distance_desc)
			_lblDistance.text = [NSString stringWithFormat:@"%@ mile", mark.distance_desc ];
		else
			_lblDistance.text = @"";
		
		if (mark.time_desc)
			_lblDuration.text = [NSString stringWithFormat:@"%@ min", mark.time_desc];
		else
			_lblDuration.text = @"";
	}
}

@end
@interface ListViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *current;
    
    NSMutableArray *markers;
    NSMutableArray *plazas;
    NSMutableArray *exits;

    int serviceType;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintStatusbar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *ListView_;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@end

@implementation ListViewController

@synthesize ListView_;
@synthesize lblTitle;

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (!self)
        return nil;
    
    [self loadMarkers];
    
    serviceType = SERVICE_ALL;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    if ([GlobalFunc getSystemVersion] < 7.0)
    {
        _constraintStatusbar.constant = 0;
    }
    
    self.ListView_.dataSource = self;
    self.ListView_.delegate = self;
	
	
}

-(void) viewWillAppear:(BOOL)animated
{
    if (self.ListView_)
    {
        switch (serviceType) {
            case SERVICE_ALL:
                lblTitle.text = @"SHOW ALL";
                current = markers;
                break;
            case SERVICE_PLAZA:
                lblTitle.text = @"SHOW PLAZA";
                current = plazas;
                break;
            case SERVICE_EXIT:
                lblTitle.text = @"SHOW EXIT";
                current = exits;
                break;
            default:
                current = markers;
                lblTitle.text = @"TIME / SPEED";
                break;
        }
        
        [self.ListView_ reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle) preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (IBAction)onTapMap:(id)sender {
    [[RootViewController shareInstance] setServiceType:serviceType which:SWITCH_MAP];
    
    [self.navigationController popViewControllerAnimated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Utillites

-(void) loadMarkers
{
    DataManager *dataMan = [DataManager shareObject];
    
    markers = [dataMan getMarkers];
    plazas = [dataMan getPlazas];
    exits = [dataMan getExits];
}


-(void) setServiceType:(int)type
{
    serviceType = type;
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [current count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //A date formatter for the time stamp
    static NSDateFormatter *dateFormatter = nil;
    if(dateFormatter == nil){
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    }
    
    static NSString *CellIdentifier = @"Cell";
    
    MarkerData *markInfo = [current objectAtIndex:indexPath.row];
    
    MarkerCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MarkerCell alloc] init];
    }
    
    [cell setMarker:markInfo service_type:serviceType];
    
    // Configure the cell...
    
    return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	MarkerData *markInfo = [current objectAtIndex:indexPath.row];
	
	if (serviceType == SERVICE_SPEEDTIME)
	{
		if ([[DataManager shareObject] calculateGeoInfoByMPH:[Config getSpeed] marker:markInfo] && markInfo.distance > 0)
		{
			NSArray *paths = [NSArray arrayWithObject:indexPath];
			
			[self.ListView_ beginUpdates];
			[self.ListView_ deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationFade];
			[self.ListView_ insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationFade];
			[self.ListView_ endUpdates];
		}else{
			[self.view makeToast:@"Cannot calculate distance.\nMaybe network is not working or you are out of Florida."];
		}
	
	}
	else{
		[[RootViewController shareInstance] setCurrentMarker:markInfo];
	
		[[RootViewController shareInstance] setServiceType:serviceType which:SWITCH_MAP];
	
		[self.navigationController popViewControllerAnimated:NO];
	}
}

#pragma mark - Google Distance API

@end
