//
//  ListViewController.h
//  TurnPike
//
//  Created by rangdang on 4/23/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"

@interface ListViewController : SuperViewController

-(void) setServiceType:(int)type;
@end

@interface MarkerCell : UITableViewCell
@end