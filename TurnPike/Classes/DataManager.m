//
//  DataManager.m
//  TurnPike
//
//  Created by rangdang on 4/24/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import "DataManager.h"
#import "Config.h"
#import "Toast+UIView.h"

#define PI		3.141592

double toRadiuns(double degree)
{
	return degree * PI / 180.f;
}

static DataManager *shareDataMgr;
static CGPoint current;

@implementation MarkerData
+(id) fromString:(NSString*)string
{
    MarkerData *object;
    
    NSArray *components = [string componentsSeparatedByString:@","];
    if (!components || [components count] < 6)
        return nil;
    
    object = [[MarkerData alloc] init];
    
    NSNumber *number;
    number = [components objectAtIndex:0];
    object.type = [number integerValue];
    object.title = [components objectAtIndex:1];
    object.marker_desc = [components objectAtIndex:2];
    object.detail = [components objectAtIndex:3];
    number = [components objectAtIndex:4];
    object.latitude = [number doubleValue];
    number = [components objectAtIndex:5];
    object.longitude = [number doubleValue];
	
	object.object = nil;
	object.notification = nil;
    return object;
}
@end

@implementation DataManager
{
    NSMutableArray *markers;
    
    NSMutableArray *plazas;
    NSMutableArray *exits;
}

+(DataManager*) shareObject
{
    if (shareDataMgr != nil)
        return shareDataMgr;
    
    shareDataMgr = [[DataManager alloc] init];
    
    return shareDataMgr;
}

+(CGFloat) getSystemVersion
{
	return [[UIDevice currentDevice].systemVersion floatValue];
}

+(CGPoint) getCurrentPosition
{
	return current;
}

+(void) setCurrentPosition:(CGPoint)position
{
	current = position;
	if (0) {
		current.x = 25.4772;
		current.y = -80.4399;
	}
}

-(BOOL) refreshGeoInfoByMPH : (long)mph Markers:(NSMutableArray*)Markers
{
	if (Markers == nil)
		Markers = plazas;
	
	for (MarkerData *mark in Markers)
	{
		if ([DataManager calcuGeoInformation:mark lat:current.x lon:current.y])
		{
			mark.time = mark.distance / mph;
			mark.time_desc = [NSString stringWithFormat:@"%ld", (long)(mark.time * 60)];
		}else
			return NO;
	}
	return YES;
}

-(BOOL) calculateGeoInfoByMPH : (long)mph marker:(MarkerData*)marker
{
	if ([DataManager calcuGeoInformation:marker lat:current.x lon:current.y])
	{
		marker.time = marker.distance / mph;
		marker.time_desc = [NSString stringWithFormat:@"%ld", (long)(marker.time * 60)];
		return YES;
	}else{
		marker.time = 0;
		marker.distance = 0;
	}
	return NO;
}

- (void) setAlarm : (MarkerData*)mark view:(UIView*)view
{
	NSString *toastStr;
	CGFloat version = [DataManager getSystemVersion];
	
	if (mark.notification == nil)
	{
		UILocalNotification *localNotif = [[UILocalNotification alloc] init];
		
		double hour = mark.time;
		double minutes = hour * 60.f;
		minutes -= [Config getAlarmMinutes];
		if (minutes < 1)
			return;
		
		double seconds = minutes * 60;
		
		NSDate *date = [[NSDate alloc] init];
		[localNotif setFireDate:[NSDate dateWithTimeInterval:seconds sinceDate:date]];
		[localNotif setTimeZone:[NSTimeZone defaultTimeZone]];
		
		localNotif.alertBody = mark.marker_desc;
//		if (version >= 8.2)
//			localNotif.alertTitle = [NSString stringWithFormat:@"MM-%@", mark.title];;
		localNotif.alertAction = [NSString stringWithFormat:@"%d", mark.nID];
		//localNotif.repeatInterval = kCFCalendarUnitSecond;
		localNotif.soundName = ALARM_SOUND_FILE;
		localNotif.applicationIconBadgeNumber += 1;
		
		[[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
		
		mark.notification = localNotif;
		
		toastStr = [NSString stringWithFormat:@"Notification set after about %ld minutes", (long)minutes];
	}
	else{
		[[UIApplication sharedApplication] cancelLocalNotification:mark.notification];
		mark.notification = nil;
		
		toastStr = [NSString stringWithFormat:@"Notification canceled for %@", [NSString stringWithFormat:@"%@", mark.marker_desc]];
	}
	if (view) [view makeToast:toastStr];
}

-(BOOL) loadData
{
    NSString *paths = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [paths stringByAppendingPathComponent:@"marks.csv"];
    NSError *error = nil;
    NSString *dataFile = [[NSString alloc] initWithContentsOfFile:bundlePath encoding:NSUTF8StringEncoding error:&error];
    
    if (error)
        return NO;
    
    if (!markers)
        markers = [[NSMutableArray alloc] init];
    else
        [markers removeAllObjects];

    if (!plazas)
        plazas = [[NSMutableArray alloc] init];
    else
        [plazas removeAllObjects];

    if (!exits)
        exits = [[NSMutableArray alloc] init];
    else
        [exits removeAllObjects];

    NSArray *dataRows = [dataFile componentsSeparatedByString:@"\r"];
    
    for (int i = 0; i < [dataRows count]; i++) {
        NSString *strRow = [dataRows objectAtIndex:i];
        
        MarkerData *object = [MarkerData fromString:strRow];
        if (!object)
            continue;
        
        object.nID = i;
        
        [markers addObject:object];
        if (object.type == MARK_PLAZA)
            [plazas addObject:object];
        else if (object.type == MARK_EXIT)
            [exits addObject:object];
        
    }
	
	NSArray *notis = [[UIApplication sharedApplication] scheduledLocalNotifications];

	for (UILocalNotification *notify in notis)
 	{
		bool bProcess = false;
		
		if ([notify.fireDate earlierDate:[[NSDate alloc] init]])
		{
			[[UIApplication sharedApplication] cancelLocalNotification:notify];
			continue;
		}
		if (notify.alertAction)
		{
			long nIndex = [notify.alertAction integerValue];
			if (nIndex > 0 && nIndex < [markers count])
			{
				MarkerData *object = [markers objectAtIndex:nIndex];
				
				object.notification = notify;
				bProcess = true;
			}
		}
		
		if (!bProcess)
		{
			[[UIApplication sharedApplication] cancelLocalNotification:notify];
		}
	}
    return YES;
}

-(NSMutableArray*) getMarkers
{
    return markers;
}

-(NSMutableArray*) getPlazas
{
    return plazas;
}

-(NSMutableArray*) getExits
{
    return exits;
}


+(double)getDistancebyCalculate : (double)lat1 lon1:(double)lon1 lat2:(double)lat2 lon2:(double)lon2
{
	if (lat1 < -90.f || lat1 > 90.f)
		return -1;
	if (lat2 < -90.f || lat2 > 90.f)
		return -1;
	
	if (lon1 < -180.f || lon1 > 180.f)
		return -1;
	if (lon2 < -180.f || lon2 > 180.f)
		return -1;
	
	double radius = 6371000;//Km
	double dLat = toRadiuns(lat2 - lat1);
	double dLon = toRadiuns(lon2 - lon1);
	
	double radLat1 = toRadiuns(lat1);
	double radLat2 = toRadiuns(lat2);
	
	double a = sin(dLat / 2) * sin(dLat / 2) + sin(dLon / 2) * sin(dLon / 2) * cos(radLat1) * cos(radLat2);
	
	double c = 2 * atan2(sqrt(a), sqrt(1-a));
	return radius * c;
}

+(double)getDistancebyGoogleApi : (double)lat1 lon1:(double)lon1 lat2:(double)lat2 lon2:(double)lon2
{
	NSString *str=[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&sensor=true",lat1, lon1, lat2, lon2];
	
	NSURL *url= [NSURL URLWithString:str];
	NSData* data = [NSData dataWithContentsOfURL:url];
	
	NSError* error;
	if (!data)
		return -1;
	NSDictionary* json = [NSJSONSerialization
						  JSONObjectWithData:data //1
						  
						  options:kNilOptions
						  error:&error];
	NSArray* arr_elements = (NSArray*)[json objectForKey:@"rows"]; //2
	NSDictionary *first=(NSDictionary*)[arr_elements objectAtIndex:0];
	NSArray *element=(NSArray*)[first valueForKey:@"elements"];
	NSDictionary *dist=(NSDictionary*)[((NSDictionary*)[element objectAtIndex:0]) objectForKey:@"distance"];
	long distance = [[dist objectForKey:@"value"] integerValue];

	return distance;
}

+(BOOL)calcuGeoInformation:(MarkerData*)marker lat:(double)orglat lon:(double)orglon
{
	double distance = 0;
	
#if 0
	distance = [self getDistancebyGoogleApi:orglat lon1:orglon lat2:marker.latitude lon2:marker.longitude];
#else
	distance = [self getDistancebyCalculate:orglat lon1:orglon lat2:marker.latitude lon2:marker.longitude];
#endif
	
	if (distance < 0)
		return NO;
	
	marker.distance = distance * 6.2137 / 10000.f;
	//marker.distance = distance / 1000.f;
	
	marker.distance_desc = [NSString stringWithFormat:@"%.2f", marker.distance];//convert miles
	marker.mile_desc = [NSString stringWithFormat:@"%ld", (long)marker.distance];//convert miles
	//	long seconds = [[time objectForKey:@"value"] integerValue];
	return YES;
}

+(NSString*) makeTimeString:(long)value
{
	NSString *strValue = @"";
	NSArray *units = @[@"s ", @"m ", @"h "];
	
	long remain;
	int level = 0;
	
	do{
		remain = value % 60;
		value = value / 60;
		
		strValue = [NSString stringWithFormat:@"%@%@", [NSString stringWithFormat:@"%ld%@", remain, [units objectAtIndex:level]], strValue];
		level ++;
	}while (value > 60 || level < 2);
	
	if (value > 0)
	{
		strValue = [NSString stringWithFormat:@"%@%@", [NSString stringWithFormat:@"%ld%@", value, [units objectAtIndex:level]], strValue];
	}
	return strValue;
}


@end

