//
//  ViewController.h
//  TurnPike
//
//  Created by b on 4/17/15.
//  Copyright (c) 2015 mica. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

enum{
    SERVICE_ALL,
    SERVICE_PLAZA,
    SERVICE_EXIT,
    SERVICE_SPEEDTIME,
};

enum{
    SWITCH_NONE,
    SWITCH_LIST,
    SWITCH_MAP,
};

@class MarkerData;

@interface RootViewController : UIViewController<CLLocationManagerDelegate>

-(void) setServiceType:(int)type which:(int)which;
- (void) setCurrentMarker:(MarkerData*)marker;
+(id) shareInstance;
@end

